from django import template
from django.core.urlresolvers import reverse

register = template.Library()

@register.filter(is_safe=True)
def button_primary(val):
    return "<button class='btn btn-primary'>%s</button>" % val

@register.simple_tag
def link_to(link, text, *args, **kwargs):
    html = "<a href='%s' {{kwargs}}>%s</a>" % (link, text)
    
    kwarg_html = ""
    
    for key, value in kwargs.items():
        kwarg_html += "%s='%s'" % (key, value)
        
    html = html.replace('{{kwargs}}', kwarg_html)
    
    return html


@register.assignment_tag
def url_as_template_var(view_name):
    return reverse(view_name)
    
@register.simple_tag
def primary_button_link(link, text):
    return "<a href='%s' class='btn btn-primary'>%s</a>" % (link, text)