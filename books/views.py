from django.shortcuts import render, get_object_or_404
from .models import Book

def books(request):
    return render(request, 'books.html', {'books': Book.objects.all()})


def book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, 'book.html', {'book': book})