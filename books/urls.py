from django.conf.urls import url
from .views import books, book

urlpatterns = [
    url(r'^$', books, name='books'),
    url(r'^description/(?P<book_id>\d+)/$', book, name='book'),
]