from django.db import models
from django.core.urlresolvers import reverse

class Book(models.Model):
    name = models.CharField(max_length=254)
    author = models.CharField(max_length=254)
    
    def get_absolute_url(self):
        return reverse('book', args={self.id})    
