from django import template

register = template.Library()

@register.simple_tag
def get_books_count(books):
    count = 0
    for book in books:
        count += 1
        
    return count

@register.inclusion_tag('book_list_item.html', takes_context=True)
def render_book_list_item(context):
    return {
        'book': context['book'],
        'book_name': context['book'].name,
    }

